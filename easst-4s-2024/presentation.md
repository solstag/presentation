# The Political Shaping of Argentine Pesticide Science<br><small>Evidence from Domain-Topic Model Analysis</small>

<img src="media/sts2024_LOGO_V2_black-cropped.svg" width="200px" style="">

Alexandre Hannud Abdo<br>(Université Gustave Eiffel - CNRS - INRAE, France; Garoa Hacker Clube, Brazil)

Scott Frickel<br>(Brown University, USA)

Florencia Arancibia<br>(CONICET - CENIT-EEyN - UNSAM, Argentina)

----
## Outline

- Context
- Motivation
- Data
- Methods
- Results

---
# Context

----

<div class="row">
<!-- https://eduambientales.net/wp-content/uploads/sites/54/2017/06/GLIFOSATO-mapa-TIEMPO.jpg -->
<div class="column">
Glyphosate map, 2009<br><img src="media/GLIFOSATO-mapa-TIEMPO.jpg" alt="Argentina, entre el glifosato y la soja / Image: Tiempo Argentino (2009)">
</div>

<div class="column">
<!-- https://agenciatierraviva.com.ar/wp-content/uploads/2020/12/paren_de_fumigar_rosario.jpg -->
<img src="media/paren_de_fumigar_rosario.jpg" alt="Páren de fumigar. Rosario / Photo: Juan Pablo Barrientos. Agencia Tierra Viva">

<!-- https://www.tiempoar.com.ar/wp-content/uploads/2021/05/31829a61cd5e1dcde060014561713ceb_BIG.jpg -->
<img src="media/damián_marino.jpg" alt="Damián Marino / Photo: Mariano Martino. Tiempo Argentino">
</div>
</div>

---
# Motivation

- The undone science of "health and environment" pesticide research in Argentina

- Science and social movements: symmetrical and co-productive interactions

  - How science has shaped the social movement is somewhat transparent

  - How social movements help shape science is much less visible

- New expertise & expert mobilisation
  - Are relational (networks), heterogeneous, boundary-spanning
  - Significant role of institutions and of "non-experts"
  - Things we mostly know from case studies, rarely as an ensemble

---
# Data

- Ethnography and field work
  - → 1,634 mobilized experts identified from field notes and attendance lists

- Bibliographic database and domain knowledge
  - → 3,304 academic articles about pesticides including a researcher in Argentina

<hr>

"Mixed-data" → 57 mobilized academic researchers

---
# Methods

## Domain-topic models
Groups of similar documents (domains) and similar terms (topics), multiple scales.

## Chained models
Groups of similar authors in terms of a domain-authorship network (profiles), with domains from a domain-topic model

## Document precocity
First (in time) among equals (in theme), at increasing scales.

---
# Results

- [Sashimi map](media/domain_map.html)

- [Network of level-4 domains and level-1 characteristic topics](#/l4d)

- [Evolution of L4D4](#/l4d4)

- [Network of level-2 domains in L4D4 and level-1 characteristic topics](#/l2d)

- [Network of level-4 domains and documents and level-1 profiles and authors for mobilized authors](#/l01e)

- [Article precocity table](#/precocity)

----
<!-- .slide: id="l4d" -->

<div class="r-stretch">
<a href="media/L4Ds.png">
<img alt="L4Ds network" src="media/L4Ds.png">
</a>
</div>

----
<!-- .slide: id="l4d4" -->

<a href="media/L4D4_evolution.png"><img alt="L4D4 evolution" src="media/L4D4_evolution.png"></a>

----
<!-- .slide: id="l2d" -->

<div class="r-stretch"><a href="media/L2Ds_in_L4D4.png"><img alt="L2Ds network of L4D4" src="media/L2Ds_in_L4D4.png"></a></div>

----
<!-- .slide: id="l01e" -->

<div class="r-stretch"><a href="media/bnm_elements_doc_author_(anonym).png"><img alt="L04D+L01E network" src="media/bnm_elements_doc_author_(anonym).png"></a></div>

----
<!-- .slide: id="precocity" -->

<a href="media/precocity table.png"><img alt="L4Ds network" src="media/precocity table.png"></a>

---
# Conclusions

Argentine pesticide research…
- Emerges steadily since the 1990's, at first disconnected from social issues
- Can be organized into domains of which the largest at level-4 roughly correspond to: analytical chemistry (L4D0), toxicology (L4D2), insect control (L4D3), control and resistance in agriculture (L4D1), human-environmental issues (L4D4)
- Closer to social movement's concerns, L4D4 gained ground in 2000 and again in 2018

Mobilized researchers…
- Are coming from almost all domains of Argentine pesticide research (10/13)
- Have published more often in domains of concern to the social movement (L4D4, L4D2)
- Are well connected through domains / domains connect well through researchers
- Have a large core that is well connected through co-authorship
- Have published articles that stand out for their precocity
  - Shaping new domains that resemble their works
  - This is particularly true for the domains of concern to the movement
  - From early days (circa 2k), including before engagement with the movement

----

# Future work

1. Citation analysis
1. Tracing institutions and locations
1. Typology of pathways
   - Further field work, follow-up interviews
   - Insight into individual experiences with political mobilization

---

# Shameless promotion

![Cortext](https://www.cortext.net/wp-content/uploads/2016/01/logo-cortext.png.webp)

Cortext Platform

https://www.cortext.net/

Have some Sashimi<small>[no fish will be harmed]</small> with Cortext Manager

https://docs.cortext.net/sashimi/

----

<strong>Obrigado and merci (from me), thanks (from Scott), gracias (from Flor)!</strong>

<br>

Alexandre Hannud Abdo

alexandre.hannud-abdo@cnrs.fr

Laboratoire Interdisciplinaire Sciences Innovations Sociétés

Université Gustave Eiffel - CNRS - INRAE

.~´

This presentation (cc-by): https://solstag.gitlab.io/presentations/easst-4s-2024/
