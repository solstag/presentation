## <span style="color:#008000">Ways-Out</span> Annual Meeting

October 2023 — Université Gustave Eiffel, France

## Explorations into the existence of <span style="color:#008000">DDD</span> as a research problem beyond the domain of transitions studies

<div class="no-bullet-lists">

- Ale Abdo (LISIS, France; CindaLab, Brazil)
- Bruno Turnheim (LISIS-INRAE, France)
- Marc Barbier (LISIS-INRAE, France)
- Benjamin Raimbault (LISIS-UGE, France)

</div>

<span style="font-size:smaller">This presentation (cc-by): https://solstag.gitlab.io/presentations/waysout2023/</span>

---

## The challenge

Inscriptions of <span style="color:green">DDD</span> vary between domains

| Transitions | Cell biology | Tobacco control | Geopolitics |
| ---------- | -------- | -------- | -------- |
| destabilisation | senescence | reduction | state failure |
| phase-out | apoptosis | behavior change | rise-and-fall |
| decline | cell death | cessation | regime change |

→ obstacle for bibliometric/textual analysis<!-- .element: class="fragment" -->

Scale of "beyond TS" → obstacle for qualitative analysis<!-- .element: class="fragment" -->

⇒ mixed-methods<!-- .element: class="fragment" -->

----

<img src="https://upload.wikimedia.org/wikipedia/commons/9/94/Jackpot_6000.jpg?20110421135538" height="200px">

<div class="table-vertical">

| Scopes           | Scales       | Hows                     | Whys         |
| --------         | --------     | --------                 | --------     |
| ⋮               | ⋮           | ⋮                        | ⋮           |
| energy           | sector       | deliberate interventions | descriptive  |
| biodiversity     | organisation | process to measure       | evaluative   |
| brexit           | region       | lived experience         | prescriptive |
| ⋮               | ⋮           | ⋮                        | ⋮           |

</div>

---

<div class="columns" style="gap: 0em; grid-template-columns: 1fr auto">

<img
    style="max-width:100%;"
    alt="corpus construction"
    src="https://md.cortext.net/uploads/90487e555387bf2b881a37721.png">

<div class="no-bullet-lists">

- ~200 articles <!-- .element: style="margin-top: 60px" -->
- ~37k articles <!-- .element: style="margin-top: 200px" -->
- ~18k articles <!-- .element: style="margin-top: 160px" -->

</div>

</div>

----

![corpus analysis](https://md.cortext.net/uploads/90487e555387bf2b881a3771c.png)


---

<div class="row">

![construction farmers](https://md.cortext.net/uploads/096509d7d912e2fe761e37305.png)

![constellation farmers](https://md.cortext.net/uploads/096509d7d912e2fe761e37306.png)

</div>

----

## Analytical results

<div style="font-size:medium; text-align:left;  display: grid; grid-template-columns: 1fr 1fr;">

<div style="border-right: solid 1px; border-bottom: solid 1px; padding-bottom: 1em"> <!-- scale -->
<strong>Scale</strong> — *applied at the level of a*

<div>
<div class="no-bullet-lists">

- FARM
  - substance, practice, technology
  - system (industry, sector, production paradigm, socio-technical)
  - system attribute, performance or output
  - economic unit (e.g. farm, organisation)
  - specific policy programmes (e.g. Ecophyto, Nitrates Directive)

</div>
<div class="no-bullet-lists">

- IPR
  - geo-political entities (nations, regions, States)
  - political regimes
  - factions (political struggle & antagonisms)
  - ideological and value systems

</div>
</div>

</div> <!-- scale -->

<div style="border-bottom: solid 1px; padding-left: 1em; padding-bottom: 1em"> <!-- scope -->
<strong>Scope</strong> — *examined within particular*

* sectors (e.g. energy, agriculture)
* societal issues (e.g. pollution, health, climate, biodiversity)
* societal spheres, political arenas

</div> <!-- scope -->

<div style="border-right: solid 1px"> <!-- how -->

<strong>How</strong> — *examined through which referentials or epistemologies?*

* DDD as contextual backdrop [no clear referential]
* DDD as difficult/rare because of path-dependences, inertia, lock-in, w/ different referentials:
    * Institutional inertia
    * Actor-political-power inertia
    * Structural inertia
* DDD as (longitudinal) process that can be traced as trajectories
* DDD as process to be measured, evaluated and compared (e.g. quantification, typologies, small-N comparisons)
* DDD with reference to deliberate (public policy) interventions
* DDD as lived experience (vulnerability, injustices, loss, conflicts)

</div> <!-- how -->

<div style="padding-left: 1em"> <!-- why -->

<strong>Why</strong> — *examined with what motivation? what’s at stake?*

<div class="columns">
<div>

* Phenomenological
    * descriptive
    * understanding
    * attributive

</div>
<div>

* Diagnostic
    * evaluative
    * scenarising
    * anticipatory

</div>
<div>

* Normative
    * alarmist
    * prescriptive
    * responsive
    * solutionist   

</div>
</div>

</div>  <!-- why -->

</div>

---

## Contributions

- Research Problems are distinct entities (≠ Research Domains), with particular attributes
- DDD exists in the social sciences, well beyond transitions studies
- We explore the existence of DDD in terms of research communities, journals, themes, sub-problems 
- Significant variety of DDD-related concepts (e.g. analytical units, referentials, motives)
- We create a research infrastructure for open investigation

----

## Available materials

- The extended then delimited <strong>corpus</strong>
- Analytical <strong>clustering</strong>: Domains, topics and journal
- <strong>Interfaces</strong>: networks, tables and sashimis

<div class="columns">

[![constellation farmers](https://md.cortext.net/uploads/096509d7d912e2fe761e37306.png)](https://hubzilla.com.br/cloud/aleabdo/research/waysout/2022-09-01/waysout-pertinent+constellation_farm%20(20220928).zip)

[![code terms table](https://md.cortext.net/uploads/90487e555387bf2b881a37723.png)](https://hubzilla.com.br/cloud/aleabdo/research/waysout/2023-02-07/regimes-code_terms-20230412.zip)

[![domain_topic_map](media/domain_topic_map.png)](https://hubzilla.com.br/cloud/aleabdo/research/waysout/2023-10-04%20presentation)

</div>

---

## What next?
#### Open invitations

Understand the DDD research problem & Lessons for Transitions Studies

- Further explore the contents and relations within the constellations we've constructed
  - FARM<sup>Farmers and agriculture</sup>, IPR <sup>Institutions, Politics, Regime change</sup>
- Explore other objects of DDD in our corpus by constructing new constellations
  - E.g. pesticides / chemical, coal / fossil
- <em>Future</em> applications producing targeted corpora, informed by our better understanding of DDD
  - E.g. focus on a particular socio-technical system: pesticides, coal

By individual researchers or through workshops with experts… (you?!)

----

## A little foraging game...

<div class="row">
<div class="no-bullet-lists" style="font-size:larger">

- Where is the paper?

<div style="margin-top: 5em"></div>

- Where is the theme?

</div>

<div class="column">

[![Flea Market from https://commons.wikimedia.org/wiki/File:MIT_Flea_Market_October_2015.agr.jpg](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Flea_market_of_Brussels_03.jpg/1024px-Flea_market_of_Brussels_03.jpg)](https://md.cortext.net/s/zChC2GWpl#)

</div>

</div>
</div>
